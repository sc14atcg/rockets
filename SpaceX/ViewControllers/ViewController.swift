//
//  ViewController.swift
//  SpaceX
//
//  Created by Andy Goldfinch on 24/03/2020.
//  Copyright © 2020 Andrew Goldfinch. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {

    var rockets: [Rocket]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        refreshData()
    }
    
    
    /**
     Load the full list of authorities from the API.
     */
    @objc func refreshData() {
        Api.getRockets {
            self.refreshControl?.endRefreshing()
            if let result = $0 {
                self.rockets = result
                self.tableView.reloadData()
            }
            else {
                //TODO display error here
            }
        }
    }
    
    
    private func configureTableView() {
        tableView.tableFooterView = UIView()
        
        self.refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.refreshData), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    
    /**
     Prepare for a segue to the detail screen by passing the rocket object.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.detail, let controller = segue.destination as? DetailViewController, let rocket = sender as? Rocket {
            controller.rocket = rocket
        }
    }

}

/**
 TableView data source & delegate methods
 */
extension MainViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rockets?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RocketCell.identifier, for: indexPath) as! RocketCell
        
        let item = rockets![indexPath.item]
        
        cell.display(rocket: item)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = rockets![indexPath.item]
        performSegue(withIdentifier: Segue.detail, sender: item)
    }
}


