//
//  DetailViewController.swift
//  SpaceX
//
//  Created by Andy Goldfinch on 24/03/2020.
//  Copyright © 2020 Andrew Goldfinch. All rights reserved.
//

import UIKit

class DetailViewController: UITableViewController {
    
    var rocket: Rocket!
    
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelStages: UILabel!
    @IBOutlet weak var labelBoosters: UILabel!
    @IBOutlet weak var labelCost: UILabel!
    @IBOutlet weak var labelFirstFlight: UILabel!
    @IBOutlet weak var labelEngineNumber: UILabel!
    @IBOutlet weak var labelEngineType: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        display(rocket: rocket)
    }

    
    /**
     Display the details of the given rocket.
     */
    func display(rocket: Rocket) {
        navigationItem.title = rocket.rocketName
        
        labelDescription.text = rocket.description
        labelStages.text = "\(rocket.stages)"
        labelBoosters.text = "\(rocket.boosters)"
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencyCode = "USD"
        numberFormatter.maximumFractionDigits = 0
        labelCost.text = numberFormatter.string(from: rocket.costPerLaunch as NSNumber)
        
        if let date = rocket.firstFlight {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            labelFirstFlight.text = formatter.string(from: date)
        }
        else {
            labelFirstFlight.text = "Unknown"
        }
        
        display(engines: rocket.engines)
    }
    
    /**
     Display the given engine details.
     */
    func display(engines: Engines) {
        labelEngineNumber.text = "\(engines.number)"
        
        switch engines.type {
        case .merlin:
            labelEngineType.text = "Merlin"
        case .raptor:
            labelEngineType.text = "Raptor"
        }
    }

}
