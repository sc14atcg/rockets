//
//  RocketCell.swift
//  SpaceX
//
//  Created by Andy Goldfinch on 24/03/2020.
//  Copyright © 2020 Andrew Goldfinch. All rights reserved.
//

import UIKit

class RocketCell: UITableViewCell {
    
    static let identifier = "RocketCell"

    @IBOutlet weak var labelName: UILabel!
    
    func display(rocket: Rocket) {
        labelName.text = rocket.rocketName
    }

}
