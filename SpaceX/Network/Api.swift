//
//  Api.swift
//  SpaceX
//
//  Created by Andy Goldfinch on 24/03/2020.
//  Copyright © 2020 Andrew Goldfinch. All rights reserved.
//

import Foundation

class Api {
    
    private init() {}

    private static let baseUrl = "https://api.spacexdata.com/v3/"


    /**
     Get a list of rockets from the SpaceX API.
     
     - parameter resultHandler: A closure to handle the network result (always called on the main thread)
     */
    static func getRockets(resultHandler: @escaping ([Rocket]?) -> Void) {
        guard let url = URL(string: baseUrl + "rockets") else {
            fatalError("Invalid URL")
        }
        
        print("🌐 Making GET request to \(url.absoluteString)")
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            let result = NetworkDecoder.decode([Rocket].self, from: data)
            DispatchQueue.main.async {
                resultHandler(result)
            }
        }
        
        task.resume()
    }

}
