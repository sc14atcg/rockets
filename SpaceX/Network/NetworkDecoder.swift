//
//  NetworkDecoder.swift
//  SpaceX
//
//  Created by Andy Goldfinch on 24/03/2020.
//  Copyright © 2020 Andrew Goldfinch. All rights reserved.
//

import Foundation

class NetworkDecoder {
    private init() {}

    static private var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        decoder.dateDecodingStrategy = .formatted(formatter)
        
        return decoder
    }()
    
    /**
     Decode a response of the given type, printing out any decoding errors.
     
     - parameter type: The type of class to decode (e.g. Movie.self)
     - parameter from: The network data containing JSON to be decoded
     */
    static func decode<T>(_ type: T.Type, from data: Data?) -> T? where T : Decodable {
        var result: T?
        
        do {
            result = try self.decoder.decode(type, from: data ?? Data())
        }
        catch {
            print("🔴 Decoding error for type \(type): \(error)\n")
        }
        
        return result
    }
}
