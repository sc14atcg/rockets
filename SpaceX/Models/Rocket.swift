//
//  Rocket.swift
//  SpaceX
//
//  Created by Andy Goldfinch on 24/03/2020.
//  Copyright © 2020 Andrew Goldfinch. All rights reserved.
//

import Foundation

struct Rocket: Codable {
    var rocketName: String
    var rocketId: String
    var description: String
    var stages: Int
    var boosters: Int
    var costPerLaunch: Int
    var firstFlight: Date?
    var engines: Engines
}

struct Engines: Codable {
    var number: Int
    var type: EngineType
}

enum EngineType: String, Codable {
    case merlin, raptor
}
