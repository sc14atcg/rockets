//
//  NetworkDecoderTests.swift
//  SpaceXTests
//
//  Created by Andy Goldfinch on 24/03/2020.
//  Copyright © 2020 Andrew Goldfinch. All rights reserved.
//

import XCTest
@testable import SpaceX

class NetworkDecoderTests: XCTestCase {

    func testDecodeRockets() throws {
        let optionalPath = Bundle(for: type(of: self)).path(forResource: "rockets", ofType: "json")
        let path = try XCTUnwrap(optionalPath, "Must be able to get path for test rockets json file")
        
        let jsonData = try Data(contentsOf: URL(fileURLWithPath: path))
        
        let rockets = NetworkDecoder.decode([Rocket].self, from: jsonData)
        
        XCTAssertNotNil(rockets, "Should be able to decode rockets list from json file")
        
        XCTAssertEqual(rockets?.count, 4, "Rockets should contain 3 authority objects")
        
        let rocket = try XCTUnwrap(rockets?.first, "Should be able to unwrap first rocket")
        
        XCTAssertEqual(rocket.rocketName, "Falcon 1", "Rocket name should match json file")
        XCTAssertEqual(rocket.rocketId, "falcon1", "Rocket id should match json file")
        XCTAssertEqual(rocket.stages, 2, "Rocket stages should match json file")
        XCTAssertEqual(rocket.boosters, 0, "Rocket boosters should match json file")
        XCTAssertEqual(rocket.costPerLaunch, 6700000, "Rocket cost should match json file")
        
        let flight = try XCTUnwrap(rocket.firstFlight, "Should be able to unwrap first flight date")
        XCTAssertEqual(Calendar.current.component(.year, from: flight), 2006, "Flight year should match json file")
        XCTAssertEqual(Calendar.current.component(.month, from: flight), 3, "Flight month should match json file")
        XCTAssertEqual(Calendar.current.component(.day, from: flight), 24, "Flight day should match json file")
    }


}
