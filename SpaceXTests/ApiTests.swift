//
//  ApiTests.swift
//  SpaceXTests
//
//  Created by Andy Goldfinch on 24/03/2020.
//  Copyright © 2020 Andrew Goldfinch. All rights reserved.
//

import XCTest
@testable import SpaceX

class ApiTests: XCTestCase {
    
    func testGetRockets() {
        let promise = expectation(description: "API will return")
        
        var result: [Rocket]?
        
        Api.getRockets {
            promise.fulfill()
            result = $0
        }
        
        waitForExpectations(timeout: 20)
                
        XCTAssertNotNil(result, "Result should not be null")
        XCTAssertEqual(result?.isEmpty, false, "Fetched rockets should not be empty")
    }

}

